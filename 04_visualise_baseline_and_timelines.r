# Visualise simulations for evolution of drug resistance over time, given
# baseline situations

# Author: Luc E. Coffeng
# Date created: 13 Feburary 2020, updated 28 July 2023

# Prep ----
rm(list = ls())
library(simresist)
library(ggplot2)
library(scales)
library(gridExtra)

theme_set(theme_bw())

base_dir <- "/Users/luc/Documents/Research/TDR/TDResist/03_Papers/02_STH_resistance"
code_dir <- file.path(base_dir, "01_Code")
output_dir <- file.path(base_dir, "02_Output")

species <- c("hookworm", "ascaris")
stopifnot(species %in% c("hookworm", "ascaris"))

setwd(output_dir)
load(file = "scenarios_main.RData")

color_palette_elim <- c("FALSE" = "black", "TRUE" = "grey")

# Prep simulation results ----------------------------------------------------
# # Load and organise output into a single data.table for whole population and save
# setwd(output_dir)
# output <- list()
# for(i in species) {
#   temp <- readRDS(file = paste0("timeline_db_main_", i, ".rds"))
#   temp <- unlist(temp, recursive = FALSE) # remove first layer of nesting
#   output <- c(output, temp)
#   rm(temp)
# }
# output <- rbindlist(lapply(output, function(x) {
#   with(x, cbind(scen,
#                 aggregate_monitor(monitor_age),
#                 baseline_id,
#                 zeta = par_input$zeta,
#                 contrib_k = par_input$contrib_k,
#                 psi = par_input$psi,
#                 species = par_input$species))
# }), use.names = TRUE, fill = TRUE)
# saveRDS(object = output, file = "output_compiled_whole_pop_main.rds")


# Add informative labels for plotting
output <- readRDS(file = "output_compiled_whole_pop_main.rds")
output[, species := factor(species,
                           levels = c("asc", "hook"),
                           labels = c("Ascaris", "Necator"))]
output[, pheno_SNP := factor(pheno_SNP,
                             levels = c("none", "recessive", "codominant", "dominant"),
                             labels = c("None", "Recessive", "Co-dominant", "Dominant"))]
output[, mda_freq := factor(mda_freq,
                            levels = c("1", "2"),
                            labels = c("Annual PC", "Biannual PC"))]
output[, mda_strategy := factor(mda_strategy,
                                levels = c("sac", "community"),
                                labels = c("School-based", "Community-based"))]
output[, trait_h2 := factor(trait_h2,
                            levels = unique(trait_h2),
                            labels = paste0("h2 = ", unique(trait_h2)))]
output[, trait_var := factor(trait,
                             levels = c("mu95_no_var",
                                        "mu95_lo_var",
                                        "mu95_hi_var"),
                             labels = c("no trait variation",
                                        "low trait variation",
                                        "high trait variation"))]

output[pheno_SNP == "None" &
         trait_h2 == "h2 = 0" &
         trait_var == "no trait variation",
       gen_mechanism := "No resistance\nmechanism"]
output[pheno_SNP == "None" &
         trait_h2 != "h2 = 0" &
         trait_var != "no trait variation",
       gen_mechanism := paste0("Polygenic\n",
                               levels(trait_h2)[trait_h2], "\n",
                               levels(trait_var)[trait_var])]
output[pheno_SNP != "None" &
         trait_h2 == "h2 = 0" &
         trait_var == "no trait variation",
       gen_mechanism := paste0("Monogenic\n",
                               levels(pheno_SNP)[pheno_SNP])]
# N.B., HARD-CODED REORDERING OF LEVELS (to check if anything changes prior to this point)
output[, gen_mechanism :=
         factor(gen_mechanism,
                levels = unique(gen_mechanism)[c(1, 5:6, 4:2)])]
output[time == 0, table(gen_mechanism, species, useNA = "always")]  # check

output[time == 0,
       transm_rate := cut(zeta,
                          breaks = c(-Inf, quantile(zeta, probs = 1:2 / 3), Inf),
                          labels = c("Low transmission",
                                     "Moderate transmission",
                                     "High transmission")),
       by = species]
output[, transm_rate := transm_rate[1], by = .(baseline_id, scen_id, species)]

output[time == 0,
       expo_het := cut(1/contrib_k,
                       breaks = c(-Inf, quantile(1/contrib_k, probs = 1:2 / 3), Inf),
                       labels = c("Low heterogeneity",
                                  "Moderate heterogeneity",
                                  "High heterogeneity")),
       by = species]
output[, expo_het := expo_het[1], by = .(baseline_id, scen_id, species)]

output[time == 0,
       egg_lifespan := cut(1/psi,
                           breaks = c(-Inf, quantile(1/psi, probs = 1:2 / 3), Inf),
                           labels = c("Short egg/larval lifespan",
                                      "Medium egg/larval lifespan",
                                      "Long egg/larval lifespan")),
       by = species]
output[, egg_lifespan := egg_lifespan[1], by = .(baseline_id, scen_id, species)]

output[time == 0,
       endem_wN := cut(n_w,
                         breaks = c(-Inf, quantile(n_w,
                                                   probs = 1:2 / 3), Inf),
                       labels = c("Small worm population size",
                                  "Moderate worm population size",
                                  "Large worm population size")),
       by = species]
output[, endem_wN := endem_wN[1], by = .(baseline_id, scen_id, species)]

output[time == 0,
       endem_load := cut(n_wf / n_host,
                         breaks = c(-Inf, quantile(n_wf / n_host,
                                                   probs = 1:2 / 3), Inf),
                         labels = c("Low average worm load",
                                    "Moderate average worm load",
                                    "High average worm load")),
       by = species]
output[, endem_load := endem_load[1], by = .(baseline_id, scen_id, species)]

output[time == 0,
       endem_prev := cut(n_host_wfpos / n_host,
                         breaks = c(-Inf, quantile(n_host_wfpos / n_host,
                                                   probs = 1:2 / 3), Inf),
                         labels = c("Low prevalence",
                                    "Moderate prevalence",
                                    "High prevalence")),
       by = species]
output[, endem_prev := endem_prev[1], by = .(baseline_id, scen_id, species)]

output[time == 0 & scen_id == 1,
       mean(n_wf / n_host),
       by = .(endem_load, species)]  # check
output[time == 0 & scen_id == 1,
       mean(n_host_wfpos / n_host),
       by = .(endem_prev, species)]  # check
output[time == 0 & scen_id == 1,
       table(endem_load, endem_prev, species)]  # check
summary(output[time == 0 & scen_id == 1,
               .(endem_prev, endem_load, transm_rate, expo_het, egg_lifespan)])

# Calculate probability of elimination per scenario
n_decimal <- 0
end_time <- output[, max(time)]
output[, elim := (max(time) < end_time | min(n_wf) == 0),
       by = .(baseline_id, scen_id, species)]

n_sim <- output[scen_id == 1 & time == 0 & species == unique(species)[1], .N]

p_elim <- output[!is.na(gen_mechanism) & time == 0,
                 .(prob = paste0("italic(P)[elim] == ",
                                 deparse(sprintf(paste0("%.", n_decimal, "f"),
                                                 round(mean(elim) * 100, n_decimal))),
                                 "*\'%\'"),
                   prob_num = round(mean(elim) * 100, n_decimal)),
                 by = .(scen_id, species,
                        mda_strategy, mda_freq, gen_mechanism)]

p_elim_table <- dcast(data = p_elim,
                      formula = species + mda_strategy + mda_freq ~ gen_mechanism,
                      value.var = "prob_num")
names(p_elim_table) <- sub("\n", " ", sub("\n", " ", names(p_elim_table)))
setwd(output_dir)
fwrite(x = p_elim_table, file = paste0("p_elim_table.csv"))

# Calculate trends in probability of having zero worms
# Note: simresist will have produced one last final survey for all
# simulations, including the subset that terminated prematurely (with 0 worms).
# For this subset, there will be a final survey at a non-integer time point
# which we have to ignore to not mess up calculation of elimination probabilities
p_elim_trend <- output[!is.na(gen_mechanism) &
                         time %in% 0:end_time,
                       .(prob = 1 - sum(n_wf > 0) / n_sim),
                       by = .(scen_id, species,
                              mda_strategy, mda_freq,
                              gen_mechanism, time)]



## Appendix B: Overview plots of baseline population states ------------------
setwd(file.path(output_dir, "01_Appendix_B"))
setkey(output, scen_id, baseline_id, time)
selection <- output[, scen_id == 1 & time == 0]

# Egg prevalence histogram
ggplot(data = output[selection],
       mapping = aes(x = n_host_eggpos / n_host * 100)) +
  geom_histogram(alpha = .5, binwidth = 5, boundary = 0) +
  scale_x_continuous(name = "\nPrevalence of eggs (%)",
                     breaks = 0:5 / 5 * 100) +
  scale_y_continuous(name = "Number of simulations\n") +
  facet_grid(~ species) +
  expand_limits(x = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_eggpos_hist.pdf")
ggsave(filename = file_name, width = 6, height = 2.8)

# Egg prevalence by transmission condition scatterplots
ggplot(data = output[selection],
       mapping = aes(x = zeta,
                     y = n_host_eggpos / n_host * 100)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nOverall transmission rate",
                breaks = 1:5 * 50,
                minor_breaks = 1:50 * 10) +
  scale_y_continuous(name = "Prevalence of eggs (%)\n",
                     breaks = 0:5 / 5 * 100) +
  facet_grid(~ species) +
  expand_limits(x = c(50, 250), y = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_eggpos_zeta.pdf")
ggsave(filename = file_name, width = 6, height = 3)

ggplot(data = output[selection],
       mapping = aes(x = contrib_k,
                     y = n_host_eggpos / n_host * 100)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nExposure heterogeneity (shape of gamma distribution)",
                breaks = c(2, 5, 10) / 10,
                minor_breaks = 2:10 / 10) +
  scale_y_continuous(name = "Prevalence of eggs (%)\n",
                     breaks = 0:5 / 5 * 100) +
  facet_grid(~ species) +
  expand_limits(x = c(.2, 1), y = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_eggpos_contrib_k.pdf")
ggsave(filename = file_name, width = 6, height = 3)

ggplot(data = output[selection],
       mapping = aes(x = 12 / psi,
                     y = n_host_eggpos / n_host * 100)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nAverage lifespan of eggs or larvae in environment (months)",
                breaks = c(.25, 0:10 / 2),
                minor_breaks = NULL) +
  scale_y_continuous(name = "Prevalence of eggs (%)\n",
                     breaks = 0:5 / 5 * 100) +
  facet_grid(~ species, scales = "free_x") +
  expand_limits(y = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_eggpos_psi.pdf")
ggsave(filename = file_name, width = 6, height = 3)

# Female worm prevalence histogram
ggplot(data = output[selection],
       mapping = aes(x = n_host_wfpos / n_host * 100)) +
  geom_histogram(alpha = .5, binwidth = 5, boundary = 0) +
  scale_x_continuous(name = "\nPrevalence of female worms (%)",
                     breaks = 0:5 / 5 * 100) +
  scale_y_continuous(name = "Number of simulations\n") +
  facet_grid(~ species) +
  expand_limits(x = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_wfpos_hist.pdf")
ggsave(filename = file_name, width = 6, height = 2.8)

# Female worm prevalence by transmission condition scatterplots
ggplot(data = output[selection],
       mapping = aes(x = zeta,
                     y = n_host_wfpos / n_host * 100)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nOverall transmission rate",
                breaks = 1:5 * 50,
                minor_breaks = 1:50 * 10) +
  scale_y_continuous(name = "Prevalence of female worms (%)\n",
                     breaks = 0:5 / 5 * 100) +
  facet_grid(~ species) +
  expand_limits(x = c(50, 250), y = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_wfpos_zeta.pdf")
ggsave(filename = file_name, width = 6, height = 3)

ggplot(data = output[selection],
       mapping = aes(x = contrib_k,
                     y = n_host_wfpos / n_host * 100)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nExposure heterogeneity (shape of gamma distribution)",
                breaks = c(2, 5, 10) / 10,
                minor_breaks = 2:10 / 10) +
  scale_y_continuous(name = "Prevalence of female worms (%)\n",
                     breaks = 0:5 / 5 * 100) +
  facet_grid(~ species) +
  expand_limits(x = c(.2, 1), y = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_wfpos_contrib_k.pdf")
ggsave(filename = file_name, width = 6, height = 3)

ggplot(data = output[selection],
       mapping = aes(x = 12 / psi,
                     y = n_host_wfpos / n_host * 100)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nAverage lifespan of eggs or larvae in environment (months)",
                breaks = c(.25, 0:10 / 2),
                minor_breaks = NULL) +
  scale_y_continuous(name = "Prevalence of female worms (%)\n",
                     breaks = 0:5 / 5 * 100) +
  facet_grid(~ species, scales = "free_x") +
  expand_limits(y = c(0, 100)) +
  theme_bw()

file_name = paste0("baseline_wfpos_psi.pdf")
ggsave(filename = file_name, width = 6, height = 3)

# Female worm load histogram
ggplot(data = output[selection],
       mapping = aes(x = n_wf / n_host)) +
  geom_histogram(alpha = .5, binwidth = 2, boundary = 0) +
  scale_x_continuous(name = "\nAverage female worm load",
                     breaks = 0:50 * 5,
                     minor_breaks = NULL) +
  scale_y_continuous(name = "Number of simulations\n") +
  facet_grid(~ species) +
  theme_bw()

file_name = paste0("baseline_wfload_hist.pdf")
ggsave(filename = file_name, width = 6, height = 2.8)

# Female worm load by transmission condition scatterplots
ggplot(data = output[selection],
       mapping = aes(x = zeta,
                     y = n_wf / n_host)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nOverall transmission rate",
                breaks = 1:5 * 50,
                minor_breaks = 1:50 * 10) +
  scale_y_continuous(name = "Average female worm load\n",
                     breaks = 0:50 * 5,
                     minor_breaks = NULL) +
  facet_grid(~ species) +
  expand_limits(x = c(50, 250)) +
  theme_bw()

file_name = paste0("baseline_wfload_zeta.pdf")
ggsave(filename = file_name, width = 6, height = 3)

ggplot(data = output[selection],
       mapping = aes(x = contrib_k,
                     y = n_wf / n_host)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nExposure heterogeneity (shape of gamma distribution)",
                breaks = c(2, 5, 10) / 10,
                minor_breaks = 2:10 / 10) +
  scale_y_continuous(name = "Average female worm load\n",
                     breaks = 0:50 * 5,
                     minor_breaks = NULL) +
  facet_grid(~ species) +
  expand_limits(x = c(.2, 1)) +
  theme_bw()

file_name = paste0("baseline_wfload_contrib_k.pdf")
ggsave(filename = file_name, width = 6, height = 3)

ggplot(data = output[selection],
       mapping = aes(x = 12 / psi,
                     y = n_wf / n_host)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nAverage lifespan of eggs or larvae in environment (months)",
                breaks = c(.25, 0:10 / 2),
                minor_breaks = NULL) +
  scale_y_continuous(name = "Average female worm load\n",
                     breaks = 0:50 * 5,
                     minor_breaks = NULL) +
  facet_grid(~ species, scales = "free_x") +
  theme_bw()

file_name = paste0("baseline_wfload_psi.pdf")
ggsave(filename = file_name, width = 6, height = 3)

# Female worm load histogram
ggplot(data = output[selection],
       mapping = aes(x = n_w)) +
  geom_histogram(alpha = .5, binwidth = 1e3, boundary = 0) +
  scale_x_continuous(name = "\nWorm population size (N)") +
  scale_y_continuous(name = "Number of simulations\n") +
  facet_grid(~ species) +
  theme_bw()

file_name = paste0("baseline_wN_hist.pdf")
ggsave(filename = file_name, width = 6, height = 2.8)

# Female worm load by transmission condition scatterplots
ggplot(data = output[selection],
       mapping = aes(x = zeta,
                     y = n_w)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nOverall transmission rate",
                breaks = 1:5 * 50,
                minor_breaks = 1:50 * 10) +
  scale_y_log10(name = "Worm population size (N)\n",
                breaks = 10^(0:10),
                minor_breaks = sapply(-5:5,
                                      function(x) (10^x) - 10^(x-1) * (1:9))) +
  facet_grid(~ species) +
  theme_bw()

file_name = paste0("baseline_wN_zeta.pdf")
ggsave(filename = file_name, width = 6, height = 4)

ggplot(data = output[selection],
       mapping = aes(x = contrib_k,
                     y = n_w)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nExposure heterogeneity (shape of gamma distribution)",
                breaks = c(2, 5, 10) / 10,
                minor_breaks = 2:10 / 10) +
  scale_y_log10(name = "Worm population size (N)\n",
                breaks = 10^(0:10),
                minor_breaks = sapply(-5:5,
                                      function(x) (10^x) - 10^(x-1) * (1:9))) +
  facet_grid(~ species) +
  expand_limits(x = c(.2, 1)) +
  theme_bw()

file_name = paste0("baseline_wN_contrib_k.pdf")
ggsave(filename = file_name, width = 6, height = 4)

ggplot(data = output[selection],
       mapping = aes(x = 12 / psi,
                     y = n_w)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_log10(name = "\nAverage lifespan of eggs or larvae in environment (months)",
                breaks = c(.25, 0:10 / 2),
                minor_breaks = NULL) +
  scale_y_log10(name = "Worm population size (N)\n",
                breaks = 10^(0:10),
                minor_breaks = sapply(-5:5,
                                      function(x) (10^x) - 10^(x-1) * (1:9))) +
  facet_grid(~ species, scales = "free_x") +
  theme_bw()

file_name = paste0("baseline_wN_psi.pdf")
ggsave(filename = file_name, width = 6, height = 4)

# Female worm load vs. prevalence scatterplot
ggplot(data = output[selection],
       mapping = aes(x = n_host_wfpos / n_host * 100,
                     y = n_wf / n_host)) +
  geom_point(shape = 1, alpha = .4) +
  scale_x_continuous(name = "\nPrevalence of female worms (%)",
                     breaks = 0:5 / 5 * 100) +
  scale_y_continuous(name = "Average female worm load\n",
                     breaks = 0:50 * 5,
                     minor_breaks = NULL) +
  facet_grid(~ species) +
  expand_limits(x = c(0, 100), y = 0) +
  theme_bw()

file_name = paste0("baseline_wfload_vs_wfpos.pdf")
ggsave(filename = file_name, width = 6, height = 2.8)


## Fig 1: Illustration of population dynamics ----
# Select species and programmatic setting
species_select <- "Necator"
mda_freq_select <- "Annual PC"
mda_strategy_select = "Community-based"

# Select genetics mechanisms to illustrate
select_genetics <-
  list("No resistance" = 
         output[, pheno_SNP == "None" & p_SNP == 0 &
                  trait_h2 == "h2 = 0" & trait == "mu95_no_var"],
       # "Polygenic" =
       #   output[, pheno_SNP == "None" & p_SNP == 0 &
       #            trait_h2 == "h2 = 0.3" & trait == "mu95_hi_var"],
       "Co-dominant" =
         output[, pheno_SNP == "Co-dominant" & p_SNP == 0.05 &
                  trait_h2 == "h2 = 0" & trait == "mu95_no_var"])

# Prep temporary object with selection of data and label genetic mechanisms
for (i in 1:length(select_genetics)) {
  output[(select_genetics[[i]]),
         genetic_label := names(select_genetics)[i]]
}
dt_subset <- copy(output[species %in% species_select &
                           mda_freq %in% mda_freq_select &
                           mda_strategy %in% mda_strategy_select &
                           !is.na(genetic_label)])
output[, genetic_label := NULL]
dt_subset[, genetic_label := factor(genetic_label, 
                                    levels = names(select_genetics))]

n_sim <- dt_subset[scen_id == unique(scen_id)[1] & time == 0, .N]
p_elim_trend_subset <- dt_subset[time %in% 0:end_time,
                                 .(prob = 1 - sum(n_w > 0) / n_sim),
                                 by = .(genetic_label, time)]

setkey(p_elim_trend_subset, genetic_label, time)
setkey(dt_subset, genetic_label, time)

n_decimal <- 0
p_elim_subset <- dt_subset[time == 0,
                           .(prob = paste0("italic(P)[elim] == ",
                                           deparse(sprintf(paste0("%.", n_decimal, "f"),
                                                           round(mean(elim) * 100, n_decimal))),
                                           "*\'%\'")),
                           by = genetic_label]

# Hack to have facet labels above different sub-plots
dt_subset[, type_wfpos := factor("Prevalence of infection")]
dt_subset[, type_wfload := factor("Average female worm load")]
dt_subset[, type_drug_efficacy := factor("Drug efficacy")]

## Create sub-plots
alpha <- 0.2
mean_line_size <- 3/4

base_plot <-
  ggplot(data = dt_subset,
         mapping = aes(x = time, group = baseline_id, col = elim)) +
  scale_x_continuous(name = NULL, breaks = 5 * 0:4, minor_breaks = 0:20) +
  scale_y_continuous(name = NULL, breaks = 20 * 0:5) +
  scale_color_manual(guide = "none", values = color_palette_elim)

# Female worm prev
plot_wfpos <- base_plot +
  geom_line(mapping = aes(y = n_host_wfpos / n_host * 100), alpha = alpha) +
  stat_summary(data = function(x) x[(!elim)],
               mapping = aes(y = n_host_wfpos / n_host * 100, group = NULL),
               fun = mean, geom = "line", col = "red",
               linewidth = mean_line_size) +
  facet_grid(genetic_label ~ type_wfpos) +
  expand_limits(y = c(0, 100)) +
  theme(strip.text.y = element_blank())

# Female worm load
plot_nwf <- base_plot + 
  scale_y_log10(name = NULL,
                breaks = 10^(-5:5),
                minor_breaks = sapply(-5:5,
                                      function(x) (10^x) - 10^(x-1) * (1:9))) +
  geom_line(mapping = aes(y = n_wf / n_host), alpha = alpha) +
  stat_summary(data = function(x) x[(!elim)],
               mapping = aes(y = n_wf / n_host, group = NULL),
               fun = mean, geom = "line", col = "red",
               linewidth = mean_line_size) +
  facet_grid(genetic_label ~ type_wfload) +
  theme(strip.text.y = element_blank())

# Drug efficacy
plot_efficacy <- base_plot +
  geom_line(mapping = aes(y = a_drug_efficacy_true * 100), alpha = alpha) +
  stat_summary(data = function(x) x[(!elim)],
               mapping = aes(y = a_drug_efficacy_true * 100, group = NULL),
               fun = mean, geom = "line", col = "red", 
               linewidth = mean_line_size) +
  facet_grid(genetic_label ~ type_drug_efficacy) +
  expand_limits(y = c(0,100)) +
  theme(strip.text.y = element_blank())

# Elimination probability
plot_elim <- ggplot(data = p_elim_trend_subset,
                    mapping = aes(x = time, y = prob * 100)) +
  geom_line(linetype = 2) +
  geom_text(data = p_elim_subset,
            mapping = aes(x = Inf, y = -Inf, label = prob),
            hjust = 1.12, vjust = -.72, parse = TRUE, size = 3) +
  scale_x_continuous(name = NULL, breaks = 5 * 0:4, minor_breaks = 0:20) +
  scale_y_continuous(name = NULL, breaks = 20 * 0:5) +
  facet_grid(genetic_label ~ "Probability of elimination") +
  expand_limits(y = 100)

# Compile plots
plot_compiled <- grid.arrange(plot_wfpos, plot_nwf, plot_efficacy, plot_elim,
                              nrow = 1,
                              bottom = "\nTime since start of PC (years)")
setwd(output_dir)
ggsave(filename = "Fig 1 - illustration.pdf",
       plot = plot_compiled,
       width = 9, height = 4.3)

rm(dt_subset, p_elim_subset)


## Appendix C: Population dynamics for all scenarios -----------------------------------
setwd(file.path(output_dir, "02_Appendix_C"))
setkey(output, elim, scen_id, baseline_id, time)

alpha <- 0.2
mean_line_size <- 3/4

for (species_select in c("Ascaris", "Necator")) {  # Ascaris or Necator
  
  selection <- output[, species == species_select & !is.na(gen_mechanism)]
  
  p_elim_trend_select <- p_elim_trend[species == species_select]
  p_elim_select <- p_elim[species == species_select]
  
  # Female worm prevalence
  ggplot(data = output[selection],
         mapping = aes(x = time, y = n_host_wfpos / n_host * 100)) +
    geom_line(mapping = aes(group = baseline_id, col = elim),
              alpha = alpha) +
    stat_summary(data = function(x) x[(!elim)],
                 fun = mean, geom = "line", col = "red",
                 linewidth = mean_line_size) +
    geom_line(data = p_elim_trend_select,
              mapping = aes(x = time, y = prob * 100),
              linetype = 2, col = "red") +
    scale_x_continuous(name = "\nTime since start of PC (years)",
                       minor_breaks = 0:20) +
    scale_y_continuous(name = "Prevalence of female worms (%)\n",
                       breaks = 0:5 / 5 * 100) +
    scale_color_manual(guide = "none",
                       values = color_palette_elim) +
    facet_grid(mda_strategy + mda_freq ~ gen_mechanism) +
    geom_text(data = p_elim_select,
              mapping = aes(x = Inf, y = Inf, label = prob),
              hjust = 1.2, vjust = 2.4, parse = TRUE, size = 2) +
    expand_limits(y = c(0, 100))
  
  file_name = paste0("timeline_", species_select, "_wfpos.pdf")
  ggsave(filename = file_name, width = 9, height = 6)
  
  # Female worm load
  ggplot(data = output[selection],
         mapping = aes(x = time, y = n_wf / n_host)) +
    geom_line(mapping = aes(group = baseline_id, col = elim),
              alpha = alpha) +
    stat_summary(data = function(x) x[(!elim)],
                 fun = mean, geom = "line", col = "red", size = mean_line_size) +
    scale_x_continuous(name = "\nTime since start of PC (years)",
                       minor_breaks = 0:20) +
    scale_y_log10(name = "Average female worm load (%)\n",
                  breaks = 10^(-5:5),
                  minor_breaks = sapply(-5:5,
                                        function(x) (10^x) - 10^(x-1) * (1:9))) +
    scale_color_manual(guide = "none",
                       values = color_palette_elim) +
    facet_grid(mda_strategy + mda_freq ~ gen_mechanism) +
    geom_text(data = p_elim_select,
              mapping = aes(x = -Inf, y = 0, label = prob),
              hjust = -.2, vjust = -.5, parse = TRUE, size = 2) +
    expand_limits(y = 0)
  
  file_name = paste0("timeline_", species_select, "_wfload.pdf")
  ggsave(filename = file_name, width = 9, height = 6)
  
  # Drug efficacy
  ggplot(data = output[selection & gen_mechanism != "No resistance\nmechanism"],
         mapping = aes(x = time, y = a_drug_efficacy_true * 100)) +
    geom_line(mapping = aes(group = baseline_id, col = elim),
              alpha = alpha) +
    stat_summary(data = function(x) x[(!elim)],
                 fun = mean, geom = "line", col = "red", size = mean_line_size) +
    scale_x_continuous(name = "\nTime since start of PC (years)",
                       minor_breaks = 0:20) +
    scale_y_continuous(name = "Drug efficacy (% of worms killed)\n",
                       breaks = 0:5 / 5 * 100) +
    scale_color_manual(guide = "none",
                       values = color_palette_elim) +
    facet_grid(mda_strategy + mda_freq ~ gen_mechanism) +
    geom_text(data = p_elim_select[gen_mechanism != "No resistance\nmechanism"],
              mapping = aes(x = -Inf, y = 0, label = prob),
              hjust = -.2, vjust = -.3, parse = TRUE, size = 2) +
    expand_limits(y = c(0, 100))
  
  file_name = paste0("timeline_", species_select, "_drug_efficacy.pdf")
  ggsave(filename = file_name, width = 9, height = 6)
  
}

rm(p_elim_select, p_elim_trend_select)


## Fig 2: Comparison of drug efficacy trends in non-eliminated settings ----
setwd(output_dir)

output[, PC_label := interaction(mda_strategy, mda_freq, lex.order = T, drop = T)]
output[, PC_label := factor(PC_label,
                            labels = c("School-based annual PC",
                                       "School-based biannual PC",
                                       "Community-based annual PC",
                                       "Community-based biannual PC"))]

ggplot(data = output[!is.na(gen_mechanism) &
                       gen_mechanism != "No resistance\nmechanism"],
       mapping = aes(x = time,
                     y = a_drug_efficacy_true * 100,
                     col = PC_label,
                     linetype = PC_label)) +
  stat_summary(data = function(x) x[(!elim)], fun = mean, geom = "line") +
  scale_x_continuous(name = "\nTime since start of PC (years)",
                     minor_breaks = 0:20) +
  scale_y_continuous(name = "Drug efficacy (% of worms killed)\n",
                     breaks = 0:5 / 5 * 100) +
  scale_color_manual(name = NULL, values = rep(c("black", "red"), each = 2)) +
  scale_linetype_manual(name = NULL, values = rep(c(1:2), times = 2)) +
  facet_grid(species ~ gen_mechanism) +
  expand_limits(y = c(0, 100))

ggsave(filename = "Fig 2 - drug efficacy by genetic mechanism.pdf",
       width = 9, height = 3.5)

output[, PC_label := NULL]


## Appendix D: Stratify results by transmission conditions ----
# Figs D1-2: probability of elimination by transmission condition
p_elim_trans <- output[!is.na(gen_mechanism) & time == 0,
                       .(prob = paste0("italic(P)[elim] == ",
                                       deparse(sprintf("%.3f",
                                                       mean(elim)))),
                         prob_num = mean(elim),
                         N_sim = .N),
                       by = .(scen_id, species,
                              mda_strategy, mda_freq, gen_mechanism,
                              endem_wN, expo_het)]

p_elim_trans[, endem_wN := factor(endem_wN,
                                  labels = c("1st", "2nd", "3rd"))]

p_elim_trans[, expo_het := factor(expo_het,
                                  labels = c("Low", "Moderate", "High"))]


p_elim_trans[, PC_label := interaction(mda_strategy,
                                       mda_freq,
                                       lex.order = T,
                                       drop = T)]
p_elim_trans[, PC_label := factor(PC_label,
                                  labels = c("School-based annual PC",
                                             "School-based biannual PC",
                                             "Community-based annual PC",
                                             "Community-based biannual PC"))]

setwd(file.path(output_dir, "03_Appendix_D"))
for (species_select in c("Ascaris", "Necator")) {

  ggplot(data = p_elim_trans[species == species_select],
         mapping = aes(x = endem_wN,
                       y = prob_num * 100,
                       fill = expo_het)) +
    geom_bar(position="dodge", stat="identity", alpha = .8, width = .5) +
    scale_x_discrete(name = "\nParasite population density (tertiles)") +
    scale_y_continuous(name = "Probability of elimination (%)\n",
                       breaks = 20 * 0:5) +
    scale_fill_manual(name = "Exposure\nheterogeneity",
                      values = c("darkgrey", "black", "red")) +
    facet_grid(mda_strategy + mda_freq ~ gen_mechanism) +
    expand_limits(y = c(0, 100)) +
    theme(legend.position = c(0.1, 0.91),
          legend.background = element_rect(fill = "white", colour = NA),
          legend.box.background = element_rect(colour = "black"),
          legend.key.size = unit(.5, 'lines'),
          legend.title = element_text(size = 8),
          legend.text = element_text(size = 8))
  
  ggsave(filename = paste0("p_elim_", species_select, "_by_Nw_and_k.pdf"),
         width = 9, height = 6)
  
}

# Figs D3-6: trends in drug efficacy by transmission condition
setwd(file.path(output_dir, "03_Appendix_D"))

output[, N_sim_wN := sum(!elim[time == 0]),
       by = .(scen_id, species,
              mda_strategy, mda_freq, gen_mechanism,
              endem_wN)]  # to identify sets with <30 non-eliminated simulations
output[, N_sim_expo_het := sum(!elim[time == 0]),
       by = .(scen_id, species,
              mda_strategy, mda_freq, gen_mechanism,
              expo_het)]  # to identify sets with <30 non-eliminated simulations

alpha <- 0.05
mean_line_size <- 3/4
N_minimum <- 30  # minimum number of non-eliminated simulations per trend line

legend_positioning <-
  theme(legend.position = c(0.1, 0.87),
        legend.background = element_rect(fill = "white", colour = NA),
        legend.box.background = element_rect(colour = "black"),
        legend.key.size = unit(.7, 'lines'),
        legend.title = element_text(size = 8),
        legend.text = element_text(size = 8))

for (species_select in c("Ascaris", "Necator")) {
 
  ggplot(data = output[(!elim) &
                         N_sim_wN >= N_minimum &
                         species == species_select &
                         gen_mechanism != "No resistance\nmechanism"],
         mapping = aes(x = time,
                       y = a_drug_efficacy_true * 100,
                       col = endem_wN)) +
    # geom_line(mapping = aes(group = baseline_id),
    #           alpha = alpha) +
    stat_summary(fun = mean, geom = "line", linewidth = mean_line_size) +
    scale_x_continuous(name = "\nTime since start of PC (years)",
                       minor_breaks = 0:20) +
    scale_y_continuous(name = "Drug efficacy (% of worms killed)\n",
                       breaks = 0:5 / 5 * 100) +
    scale_color_manual(name = "Parasite population\ndensity (tertiles)",
                      values = c("darkgrey", "black", "red"),
                      labels = c("1st", "2nd", "3rd")) +
    facet_grid(mda_strategy + mda_freq ~ gen_mechanism) +
    expand_limits(y = c(0, 100)) +
    legend_positioning
   
  ggsave(filename = paste0("drug_efficacy_", species_select, "_by_Nw.pdf"),
         width = 9, height = 6)
  
  
  ggplot(data = output[(!elim) &
                         N_sim_expo_het >= N_minimum &
                         species == species_select &
                         gen_mechanism != "No resistance\nmechanism"],
         mapping = aes(x = time,
                       y = a_drug_efficacy_true * 100,
                       col = expo_het)) +
    # geom_line(mapping = aes(group = baseline_id),
    #           alpha = alpha) +
    stat_summary(fun = mean, geom = "line", linewidth = mean_line_size) +
    scale_x_continuous(name = "\nTime since start of PC (years)",
                       minor_breaks = 0:20) +
    scale_y_continuous(name = "Drug efficacy (% of worms killed)\n",
                       breaks = 0:5 / 5 * 100) +
    scale_color_manual(name = "Exposure heterogeneity\n(tertiles)",
                       values = c("darkgrey", "black", "red"),
                       labels = c("1st", "2nd", "3rd")) +
    facet_grid(mda_strategy + mda_freq ~ gen_mechanism) +
    expand_limits(y = c(0, 100)) +
    legend_positioning
  
  ggsave(filename = paste0("drug_efficacy_", species_select, "_by_k.pdf"),
         width = 9, height = 6)
  
}

output[, c("N_sim_wN", "N_sim_expo_het") := NULL]


# # OLD CODE used to explore results for Appendix D (might be useful later) ----
# # Define convenience functions for plotting trends in non-eliminated simulations
# # and the associated elimination probability
# # Instruction: supply `col_vars` and `row_vars` as character values of the form
# # "endem_load" and/or "endem_load + gen_mechanism" (use "+") if you want to
# # stratify rows and/or columns by multiple variables
# gen_plot_wprev <- function(x, col_vars = NULL, row_vars = NULL,
#                            size_def = 2) {
#   
#   dt_by_cols <- paste(col_vars, row_vars, sep = ",")
#   while (grepl("\\+| \\+ | \\+|\\+ ", dt_by_cols)) {
#     dt_by_cols <- sub("\\+| \\+ | \\+|\\+ ", ",", dt_by_cols)
#   }
#   
#   p_elim <- x[time == 0,
#               .(prob = paste0("italic(P)[elim] == ",
#                               deparse(sprintf("%.3f",
#                                               mean(elim))))),
#               by = dt_by_cols]
#   
#   ggplot(data = x[elim == FALSE]) +
#     geom_line(mapping = aes(y = n_host_wfpos / n_host * 100,
#                             x = time,
#                             group = baseline_id),
#               alpha = 0.3) +
#     scale_x_continuous(name = "\nTime since start of PC (years)",
#                        minor_breaks = 0:20) +
#     scale_y_continuous(name = "Prevalence of female worms (%)\n",
#                        breaks = 0:5 / 5 * 100) +
#     geom_text(data = p_elim,
#               mapping = aes(x = -Inf, y = -Inf, label = prob),
#               hjust = -0.1, vjust = -0.2, parse = TRUE,
#               size = size_def) +
#     facet_grid(as.formula(paste(row_vars, col_vars, sep = "~"))) +
#     expand_limits(y = c(0, 100))
# }
# 
# gen_plot_wload <- function(x, col_vars = NULL, row_vars = NULL,
#                            size_def = 2) {
#   
#   dt_by_cols <- paste(col_vars, row_vars, sep = ",")
#   while (grepl("\\+| \\+ | \\+|\\+ ", dt_by_cols)) {
#     dt_by_cols <- sub("\\+| \\+ | \\+|\\+ ", ",", dt_by_cols)
#   }
#   
#   p_elim <- x[time == 0,
#               .(prob = paste0("italic(P)[elim] == ",
#                               deparse(sprintf("%.3f",
#                                               mean(elim))))),
#               by = dt_by_cols]
#   
#   ggplot(data = x[elim == FALSE]) +
#     geom_line(mapping = aes(y = n_wf / n_host,
#                             x = time,
#                             group = baseline_id),
#               alpha = 0.3) +
#     scale_x_continuous(name = "\nTime since start of PC (years)",
#                        minor_breaks = 0:20) +
#     scale_y_log10(name = "Average female worm load (%)\n",
#                   breaks = 10^(-5:5),
#                   minor_breaks = c(sapply(-5:5,
#                                           function(x) (10^x) - 10^(x-1) * (1:9)))) +
#     geom_text(data = p_elim,
#               mapping = aes(x = 0, y = 0, label = prob),
#               hjust = -0.1, vjust = -0.2, parse = TRUE,
#               size = size_def) +
#     facet_grid(as.formula(paste(row_vars, col_vars, sep = "~")))
# }
# 
# gen_plot_efficacy <- function(x, col_vars = NULL, row_vars = NULL,
#                               size_def = 2) {
#   
#   dt_by_cols <- paste(col_vars, row_vars, sep = ",")
#   while (grepl("\\+| \\+ | \\+|\\+ ", dt_by_cols)) {
#     dt_by_cols <- sub("\\+| \\+ | \\+|\\+ ", ",", dt_by_cols)
#   }
#   
#   p_elim <- x[time == 0,
#               .(prob = paste0("italic(P)[elim] == ",
#                               deparse(sprintf("%.3f",
#                                               mean(elim))))),
#               by = dt_by_cols]
#   
#   ggplot(data = x[elim == FALSE]) +
#     geom_line(mapping = aes(y = a_drug_efficacy_true * 100,
#                             x = time,
#                             group = baseline_id),
#               alpha = 0.3) +
#     stat_summary(mapping = aes(y = a_drug_efficacy_true * 100,
#                                x = time),
#                  fun = mean, geom = "line",
#                  col = "red") +
#     scale_x_continuous(name = "\nTime since start of PC (years)",
#                        minor_breaks = 0:20) +
#     scale_y_continuous(name = "Drug efficacy (% of worms killed)\n",
#                        breaks = 0:5 / 5 * 100) +
#     geom_text(data = p_elim,
#               mapping = aes(x = -Inf, y = -Inf, label = prob),
#               hjust = -0.1, vjust = -0.4, parse = TRUE,
#               size = size_def) +
#     facet_grid(as.formula(paste(row_vars, col_vars, sep = "~"))) +
#     expand_limits(y = c(0, 100))
# }
# 
# 
# # Select data to plot
# setwd(file.path(output_dir, "03_Appendix_D"))
# setkey(output, species, scen_id, endem_load, endem_prev, time)
# 
# scen_id_select <- scen[mda_freq == 1 & mda_strategy == "community", scen_id]
# selection <- output[, scen_id %in% scen_id_select &
#                       # gen_mechanism == "Monogenic\nCo-dominant" &
#                       ((gen_mechanism == "Monogenic\nCo-dominant" &
#                           species == "Ascaris") |
#                          (gen_mechanism == "Monogenic\nCo-dominant" &
#                             species == "Necator"))]
# 
# 
# # CONCL: higher risk/speed in larger worm populations ----
# gen_plot_efficacy(output[selection], "endem_wN", "species", 3)
# ggsave(filename = "efficacy_trend_wN.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection], "endem_load", "species", 3)
# ggsave(filename = "efficacy_trend_wload.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection], "endem_prev", "species")
# ggsave(filename = "efficacy_trend_wprev.pdf", height = 5, width = 6.5)
# 
# 
# # CONCL: higher risk/speed with higher transmission rates ----
# gen_plot_efficacy(output[selection], "transm_rate", "species")
# ggsave(filename = "efficacy_trend_zeta.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection & endem_wN == "Large worm population size"],
#                   "transm_rate", "species")
# ggsave(filename = "efficacy_trend_zeta_high wN.pdf", height = 5, width = 6.5)
# 
# gen_plot_efficacy(output[selection & species == "Ascaris"],
#                   "transm_rate", "endem_wN")
# ggsave(filename = "efficacy_trend_zeta_Ascaris_wN.pdf", height = 7, width = 6.5)
# gen_plot_efficacy(output[selection & species == "Necator"],
#                   "transm_rate", "endem_wN")
# ggsave(filename = "efficacy_trend_zeta_Necator_wN.pdf", height = 7, width = 6.5)
# 
# 
# # CONCL: higher risk/speed in more highly aggregated worm populations ----
# gen_plot_efficacy(output[selection], "expo_het", "species")
# ggsave(filename = "efficacy_trend_k.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection & endem_wN == "Large worm population size"],
#                   "expo_het", "species")
# ggsave(filename = "efficacy_trend_k_high wN.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection & species == "Ascaris"],
#                   "expo_het", "endem_wN")
# ggsave(filename = "efficacy_trend_k_Ascaris_wN.pdf", height = 7, width = 6.5)
# gen_plot_efficacy(output[selection & species == "Necator"],
#                   "expo_het", "endem_wN")
# ggsave(filename = "efficacy_trend_k_Necator_wN.pdf", height = 7, width = 6.5)
# 
# 
# # CONCL: no strong impact of egg/larval lifespan in the environment ----
# # Hypothesis: two effects balance each other out: longer egg/larval lifespan
# # results in 1) a larger adult worm population (and thus higher speed of
# # evolution), and 2) a larger reservoir of old genetic material that slows down
# # replacement of wildtype alleles with resistant alleles.
# gen_plot_efficacy(output[selection], "egg_lifespan", "species")
# ggsave(filename = "efficacy_trend_psi.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection & endem_wN == "Moderate worm population size"],
#                   "egg_lifespan", "species")
# ggsave(filename = "efficacy_trend_psi_mod wN.pdf", height = 5, width = 6.5)
# gen_plot_efficacy(output[selection & species == "Ascaris"],
#                   "egg_lifespan", "endem_wN")
# ggsave(filename = "efficacy_trend_psi_Ascaris_wN.pdf", height = 7, width = 6.5)
# gen_plot_efficacy(output[selection & species == "Necator"],
#                   "egg_lifespan", "endem_wN")
# ggsave(filename = "efficacy_trend_psi_Necator_wN.pdf", height = 7, width = 6.5)
# 
# 
### END OF CODE ### ----
