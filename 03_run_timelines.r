# Run simulations for evolution of drug resistance over time, given baseline
# situations

# Author: Luc E. Coffeng
# Date created: 6 Feburary 2020

# Prep
rm(list = ls())
library(simresist)
library(ggplot2)
library(foreach)
library(doParallel)
library(boot)

base_dir <- "/Users/luc/Documents/Research/TDR/TDResist/03_Papers/02_STH_resistance"
code_dir <- file.path(base_dir, "01_Code")
output_dir <- file.path(base_dir, "02_Output")

n_cores <- 8 #detectCores(logical = FALSE)
run_species <- c("hookworm", "ascaris")
stopifnot(all(run_species %in% c("hookworm", "ascaris")))


### Set up intervention scenarios ----
# Specify inputs
n_sim_max <- 1e3
sim_steps <- 52
sim_time <- 20L
survey_freq <- 1
reset_genetics_time <- 0

scen_par <- list(
  mda_freq = c(1, 2),
  mda_strategy = list("sac" = list(mda_age = c(5, 15), mda_cov = 0.95),
                      "community" = list(mda_age = c(2, Inf), mda_cov = 0.70)),
  
  p_SNP = list(0, .05),
  pheno_SNP = list("dominant" = c(1, .1, .1),    # i.e. drug efficacy for "aa",
                   "codominant" = c(1, .6, .1),  # "Aa", and "AA" relative to a
                   "recessive" = c(1, 1, .1),    # non-resistant haplotype ("aa")
                   "none" = c(1, 1, 1)),         # (look for definition of 'pheno_kill' to see)
  
  trait_h2 = c(0, .3) ,
  trait_prop_t = 1.0,
  trait = list("mu95_no_var" = list("kill_prob_mu" = .95, "kill_prob_lo" = .95),
               "mu95_lo_var" = list("kill_prob_mu" = .95, "kill_prob_lo" = .90),
               "mu95_hi_var" = list("kill_prob_mu" = .95, "kill_prob_lo" = .80))
)

# Convert desired trait properties to input parameters for simresist, i.e., mean
# probability of a worm being killed and variance in log-odds of being killed.
scen_par$trait <- lapply(scen_par$trait, function(input) {
  
  if (with(input, kill_prob_mu == kill_prob_lo)) {
    
    list("kill_prob_mu" = input$kill_prob_mu,
         "trait_var" = 0)
    
  } else {
    
    SSE <- function(logit_prob_mu_input,  # mean log-odds to be calibrated
                    prob_lo_input,        # to analytically derive sd on log-odds scale
                    prob_mu_target) {     # target mean on probability scale
      
      logit_prob_sd <- abs((logit_prob_mu_input - logit(prob_lo_input)) /
                             qnorm(p = .025, mean = 0, sd = 1))
      
      n_q <- 1e5L
      q <- (1:(n_q - 1)) / n_q
      prob_mu_output <- mean(inv.logit(qnorm(p = q,
                                             mean = logit_prob_mu_input,
                                             sd = logit_prob_sd)))
      
      return((prob_mu_output - prob_mu_target)^2)
    }
    
    logit_prob_mu_hat <- optim(par = c("logit_prob_mu" = logit(input$kill_prob_mu)),
                               fn = SSE,
                               prob_mu_target = input$kill_prob_mu,
                               prob_lo_input = input$kill_prob_lo,
                               method = "BFGS",
                               control = list(maxit = 1e3))$par
    logit_prob_sd_hat <- unname(abs((logit_prob_mu_hat -
                                       logit(input$kill_prob_lo)) /
                                      qnorm(p = .025, mean = 0, sd = 1)))
    
    list("kill_prob_mu" = unname(inv.logit(logit_prob_mu_hat)),
         "trait_var" = logit_prob_sd_hat^2)
    
  }
  
})

# Check that trait input parameters are as expected
lapply(scen_par$trait, function(input) {
  with(input, {
    x <- qnorm(p = (1:(1e5-1)) / 1e5,
               mean = logit(kill_prob_mu),
               sd = sqrt(trait_var))
    c("prob_mu" = mean(inv.logit(x)),
      "prob_lo" = inv.logit(qnorm(.025, mean = logit(kill_prob_mu), sd = sqrt(trait_var))),
      "prob_hi" = inv.logit(qnorm(.975, mean = logit(kill_prob_mu), sd = sqrt(trait_var))))
  })
})

# Construct combinations of all scenario parameters
scen <- as.data.table(with(scen_par,
                           expand.grid("mda_freq" = mda_freq,
                                       "mda_strategy" = names(mda_strategy),
                                       "p_SNP" = p_SNP,
                                       "pheno_SNP" = names(pheno_SNP),
                                       "trait_h2" = trait_h2,
                                       "trait_prop_t" = trait_prop_t,
                                       "trait" = names(trait),
                                       stringsAsFactors = FALSE)))
scen <- scen[, lapply(.SD, unlist)]  # remove list aspect (needed for unique())

# Remove combinations of polygenic and monogenic inheritance (we will focus on
# each separately)
scen[p_SNP != 0 | pheno_SNP != "none",
     c("trait_h2", "trait") := list(0, "mu95_no_var")]
scen[trait_h2 != 0 | trait != "mu95_no_var",
     c("p_SNP", "pheno_SNP") := list(0, "none")]
scen <- unique(scen)

# Remove non-sensical and effectively duplicate scenarios
scen[p_SNP == 0, pheno_SNP := "none"]
scen[pheno_SNP == "none", p_SNP := 0]
scen[trait == "mu95_no_var", trait_h2 := 0]
scen[trait_h2 == 0, trait := "mu95_no_var"]
scen <- unique(scen)


# Construct data.table with input parameters, using the input parameter
# nomenclature for simresist()
scen[, scen_id := .I]
scen_par_input <-
  scen[, .("mda_time" = list(reset_genetics_time +
                               (0:((sim_time - reset_genetics_time) *
                                     mda_freq)) / mda_freq),
           "mda_age" = list(with(scen_par$mda_strategy,
                                 get(mda_strategy)$mda_age)),
           "mda_cov" = with(scen_par$mda_strategy,
                            get(mda_strategy)$mda_cov),
           "p_SNP" = p_SNP,
           "pheno_kill" = list(with(scen_par$pheno_SNP,
                                    get(pheno_SNP)) *
                                 with(scen_par$trait,
                                      get(trait)$kill_prob_mu)),
           "trait_h2" = trait_h2,
           "trait_var" = with(scen_par$trait,
                              get(trait)$trait_var),
           "trait_prop_t" = trait_prop_t),
       by = scen_id]

setwd(output_dir)
save(scen, scen_par_input, file = "scenarios_main.RData")


### Perform the simulations for each specified species ----
for (species in run_species) {
  
  ## Prep species-specific baseline population states ----
  # Load baseline database
  setwd(output_dir)
  baseline <- readRDS(file = paste0("baseline_db_", species, ".rds"))
  
  # Discarding simulations that where terminated prematurely because of zero worms
  end_times <- sapply(baseline, function(x) x$end_time)
  end_time_select <- max(end_times)
  baseline <- lapply(baseline, function(x) {
    if (x$end_time == end_time_select && x$state$worms[dead == 0, .N] > 1) {
      return(x)
    } else (
      NULL
    )
  })
  baseline[sapply(baseline, is.null)] <- NULL
  if(length(baseline) > n_sim_max) {
    baseline <- baseline[1:n_sim_max]
  }
  
  # Prep for simulations
  n_baseline <- length(baseline)
  baseline_states <- lapply(baseline, function(x) x$state)
  baseline_param <- lapply(baseline, function(x) x$param)
  rm(baseline)
  
  ## Run simulations for all scenarios and baseline states ----------------------
  # Prep for parallel simulation
  cluster <- makeCluster(n_cores)
  registerDoParallel(cluster)
  
  # Initiate log file
  setwd(output_dir)
  writeLines(paste0(Sys.time(), ": Starting simulations for ",
                    n_baseline, " baseline scenarios and ",
                    scen[, .N], " intervention scenarios."),
             "log.txt") # initiate log file
  
  output <-
    foreach(i = 1:n_baseline) %:%
    foreach(j = 1:scen_par_input[, .N],
            .packages = c("data.table", "simresist"),
            .inorder = FALSE,
            .errorhandling = "pass") %dopar% {
              
              setDTthreads(threads = 1) # avoid each worker using all cores
              
              # Update log files
              setwd(output_dir)
              sink("log.txt", append = TRUE)
              cat(paste0(Sys.time(), ": Starting baseline scenario ", i,
                         " + intervention scenario ", j, "\n"))
              sink()
              
              # Collate parameters for simulation
              if(species == "hookworm") {
                sim_species <- "hook"
              }
              if(species == "ascaris") {
                sim_species <- "asc"
              }
              
              param_collated <- c(baseline_param[[i]],
                                  unlist(as.list(scen_par_input[j])[-1],  # drop the scen_id
                                         recursive = FALSE))
              param_collated$runtime <- sim_time
              param_collated$steps <- sim_steps
              param_collated$survey_freq <- survey_freq
              param_collated$species <- sim_species
              param_collated$reset_genetics_time <- reset_genetics_time
              
              # Make sure that the genetics for each baseline scenario are reset
              # to exactly the same values for all the intervention scenarios
              set.seed(12345678 + i)
              
              # Run simulation
              prediction <- do.call(what = simresist,
                                    args = c(list(state_init = baseline_states[[i]]),
                                             param_collated))
              
              # Return selected output
              list(baseline_id = i,
                   scen = scen[j],
                   par_input = param_collated,
                   monitor_age = prediction$monitor_age,
                   drug_efficacy = prediction$drug_efficacy)
              
            }
  
  sink("log.txt", append = TRUE)
  cat(paste0(Sys.time(), ": finished simulations\n"))
  sink()
  
  stopCluster(cluster)
  
  setwd(output_dir)
  saveRDS(object = output, file = paste0("timeline_db_main_", species, ".rds"))
  
}

### END OF CODE ### ------------------------------------------------------------
