# Create a database of baseline population states with the simresist package

# Author: Luc E. Coffeng
# Date created: 4 Feburary 2020

# Prep
rm(list = ls())
library(simresist)
library(ggplot2)
library(lhs)
library(foreach)
library(doParallel)

base_dir <- "/Users/luc/Documents/Research/TDR/TDResist/03_Papers/02_STH_resistance"
code_dir <- file.path(base_dir, "01_Code")
output_dir <- file.path(base_dir, "02_Output")


# Set up baseline transmission scenarios
n_scen <- 1500
human_warmup <- 250L
worm_warmup <- 50L
survey_freq <- 1 / worm_warmup
sim_steps <- 52
par_limits_log <- lapply(list("log_zeta" = c(50, 250),
                              "log_contrib_k" = c(0.2, 1.0),
                              "log_psi" = 52 / c(1, 8)), log)
# N.B.: following code assumes par_limits_log is on the log scale!
set.seed(87654321)
par_quantiles <- maximinLHS(n = n_scen, k = length(par_limits_log))
colnames(par_quantiles) <- names(par_limits_log)
scen <- sapply(names(par_limits_log), function(i) {
  with(par_limits_log, get(i)[1] + par_quantiles[, i] * diff(get(i)))
  })
scen <- as.data.table(exp(scen))
setnames(x = scen,
         old = names(scen),
         new = sub("log_", "", names(scen)))


# Run simulations in parallel
n.cores <- 8 #detectCores(logical = TRUE)
cluster <- makeCluster(n.cores)
registerDoParallel(cluster)

setwd(output_dir)
writeLines(paste0(Sys.time(), ": Starting simulations for ",
                  scen[, .N], " scenarios."),
           "log.txt") # initiate log file

output <- foreach(i = 1:n_scen,
                  .packages = c("data.table", "simresist"),
                  .inorder = FALSE,
                  .errorhandling = "pass") %dopar% {

  setDTthreads(threads = 1) # avoid each worker using all cores

  setwd(output_dir)
  sink("log.txt", append = TRUE)
  cat(paste0(Sys.time(), ": Starting scenario ", i, "\n"))
  sink()

  set.seed(12345678 + i)
  do.call(what = simresist,
          args = c(list(state_init = NULL,
                        runtime = worm_warmup,
                        steps = sim_steps,
                        survey_freq = survey_freq,
                        species = "hook",
                        mda_time = -1,
                        human_warmup_duration = human_warmup),
                   as.list(scen[i])))

                  }

sink("log.txt", append = TRUE)
cat(paste0(Sys.time(), ": finished simulations\n"))
sink()

stopCluster(cluster)

setwd(output_dir)
# saveRDS(object = output, file = "baseline_db_hookworm.rds")


# Quick exploration
setwd(output_dir)
output <- readRDS(file = "baseline_db_hookworm.rds")

output_summary <- rbindlist(lapply(output, function(x) {

  cbind(x$monitor_pop[, .(zeta = x$param$zeta,
                          contrib_k = x$param$contrib_k,
                          psi = x$param$psi,
                          time,
                          p_egg_pop = n_host_eggpos / n_host,
                          p_w_pop = n_host_wfpos / n_host)],
        x$monitor_age[age_cat >= 5 & age_cat < 15,
                      .(p_egg_sac = n_host_eggpos / n_host,
                        p_w_sac = n_host_wfpos / n_host)])

}))

qplot(data = output_summary[time == max(time)], x = zeta, y = p_egg_sac,
      geom = "point") + expand_limits(y = 0)
qplot(data = output_summary[time == max(time)], x = contrib_k, y = p_egg_sac,
      geom = "point") + expand_limits(y = 0)
qplot(data = output_summary[time == max(time)], x = 52 / psi, y = p_egg_sac,
      geom = "point") + expand_limits(y = 0)



### END OF CODE ###
